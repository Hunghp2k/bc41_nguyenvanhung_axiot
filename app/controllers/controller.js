function batLoading() {
    document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
    document.getElementById("loading").style.display = "none";
}

function renderQLNDList(QLNDArr) {
    var contentHTML = "";
    QLNDArr.forEach((QLND) => {
        contentHTML += `
          <tr>
           <td>${QLND.id}</td>       
           <td>${QLND.taiKhoan}</td>       
           <td>${QLND.matKhau}</td>       
           <td>${QLND.hoTen}</td>       
           <td>${QLND.email}</td>       
           <td>${QLND.ngonNgu}</td>       
           <td>${QLND.loaiND}</td>       
           <td>
           <button onclick='xoaNguoiDung(${QLND.id})' class='btn btn-danger  '>Xoá</button>
           <button data-toggle="modal"
           data-target="#myModal" onclick='suaNguoiDung(${QLND.id})' class='btn btn-warning w-50 '>Sửa</button>
           </td>       
          </tr>
        `;
    });
    return (document.getElementById("tblDanhSachNguoiDung").innerHTML =
        contentHTML);
}

function layThongTinTuFrom() {
    var id = document.getElementById("id").value;
    var taiKhoan = document.getElementById("TaiKhoan").value;
    var hoTen = document.getElementById("HoTen").value;
    var matKhau = document.getElementById("MatKhau").value;
    var email = document.getElementById("Email").value;
    var hinhAnh = document.getElementById("HinhAnh").value;
    var loaiND = document.getElementById("loaiNguoiDung").value;
    var ngonNgu = document.getElementById("loaiNgonNgu").value;
    var moTa = document.getElementById("MoTa").value;
    return {
        id: id,
        taiKhoan: taiKhoan,
        hoTen: hoTen,
        matKhau: matKhau,
        email: email,
        hinhAnh: hinhAnh,
        loaiND: loaiND,
        ngonNgu: ngonNgu,
        moTa: moTa,
    };
}

function showThongTinLenFrom(QLND) {
    document.getElementById("id").value = QLND.id;
    document.getElementById("TaiKhoan").value = QLND.taiKhoan;
    document.getElementById("HoTen").value = QLND.hoTen;
    document.getElementById("MatKhau").value = QLND.matKhau;
    document.getElementById("Email").value = QLND.email;
    document.getElementById("HinhAnh").value = QLND.hinhAnh;
    document.getElementById("loaiNguoiDung").value = QLND.loaiND;
    document.getElementById("loaiNgonNgu").value = QLND.ngonNgu;
    document.getElementById("MoTa").value = QLND.moTa;
}

